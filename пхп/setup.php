<?php
require_once 'connection.php';
 
$link = mysqli_connect($host, $user, $password, $database) 
    or die ("Error" . mysqli_error($link));

echo "You connected successfully ";

$sql = "CREATE TABLE client (
    id_client int(10) not null,
    book varchar(25) not null,
    book_id_book int(10) not null,
    department_id_department int(10) not null,
    primary key(id_client),
    foreign key(book_id_book) references book(id_book),
    foreign key(department_id_department) references department(id_department)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "CREATE TABLE order_order (
    id_order int(10) not null,
    author_id_author int(10) not null,
    date varchar(25) not null,
    book varchar(25) not null,
    client_id_client int(10) not null,
    book_id_book int(10) not null,
    primary key(id_order),
    foreign key(author_id_author) references author(id_author),
    foreign key(client_id_client) references client(id_client),
    foreign key(book_id_book) references book(id_book)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "CREATE TABLE book (
    id_book int(10) not null,
    title varchar(50) not null,
    author varchar(50) not null,
    year int(10) not null,
    publisher varchar(25) not null,
    primary key(id_book)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "CREATE TABLE author (
    id_author int(10) not null,
    fio_author varchar(50) not null,
    primary key(id_author)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "CREATE TABLE manager (
    id_manager int(10) not null,
    department_id_department int(10) not null,
    fio_manager varchar(45) not null,
    salary_sel int(10) not null,
    salary_clean int(10) not null,
    seller_id_seller int(10) not null,
    cleaner_id_cleaner int(10) not null,
    primary key(id_manager),
    foreign key(department_id_department) references department(id_department),
    foreign key(seller_id_seller) references seller(id_seller),
    foreign key(cleaner_id_cleaner) references cleaner(id_cleaner)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "CREATE TABLE department (
    id_department int(10) NOT NULL,
    name varchar(50) NOT NULL,
    age_limits varchar(10) not null,
    book_id_book int(10),
    primary key(id_department)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "CREATE TABLE seller (
    id_seller int(10) not null,
    department_id_department int(10) not null,
    fio_seller varchar(45) not null,
    primary key(id_seller)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "CREATE TABLE cleaner(
    id_cleaner int(10) not null,
    fio_cleaner varchar(35) not null,
    time varchar(25) not null,
    day_of_the_week varchar(25) not null,
    primary key(id_cleaner)
    )";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}



$sql = "INSERT INTO client VALUES 
    ('100', 'Преступление и наказание', '1200', '20'), 
    ('101', 'Старик и море', '1000', '21'),
    ('102', 'Вокруг света за 80 дней', '2000', '21'),
    ('103', 'Три мушкетерв', '3000', '21'),
    ('104', 'Повелитель мух', '4000', '21'),
    ('106', 'По ком звонит колокол', '1300', '21')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "INSERT INTO order_order VALUES 
    ('111', '1', '23.04.2018', 'Война и мир', '104', '6000'), 
    ('112', '2', '14.06.2016', 'Темные аллеи', '102', '5000'),
    ('113', '3', '28.08.2017', 'Дети капитана Гранта', '103', '1100'),
    ('114', '4', '15.05.2015', 'Машина времени', '102', '9000'),
    ('115', '9', '28.08.2017', 'Затерянный мир', '100', '8000'),
    ('116', '3', '13.09.2017', 'Таинственный остров', '106', '1300')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "INSERT INTO book VALUES 
    ('1200', 'Преступление и наказание', 'Достоевский', '2016', 'Звезда'),
    ('1000', 'Старик и море', 'Хэмингуэй', '2016', 'Звезда'),
    ('2000', 'Вокруг света за 80 дней', 'Верн', '2018', 'Синяя птица'),
    ('3000', 'Три мушкетера', 'Дюма', '2016', 'Звезда'),
    ('4000', 'Повелитель мух', 'Голдинг', '2017', 'Дрофа'),
    ('5000', 'Темные аллеи', 'Бунин', '2016', 'Звезда'),
    ('6000', 'Война и мир', 'Толстой', '2018', 'Синяя птица'),
    ('7000', 'Путешествие к центру Земли', 'Верн', '2013', 'Дрофа'),
    ('8000', 'Затерянный мир ', 'Дойл', '2016', 'Звезда'),
    ('9000', 'Машина времени', 'Уэллс', '2016', 'Звезда'),
    ('1100', 'Дети капитана Гранта', 'Верн', '2014', 'Дрофа'),
    ('1300', 'По ком звонит колокол', 'Хэмингуэй', '2014', 'Синяя птица')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "INSERT INTO author VALUES 
    ('1', 'Толстой Л'), 
    ('2', 'Бунин И'), 
    ('3', 'Верн Ж'),
    ('4', 'Уэллс Г'), 
    ('5', 'Достоевский Ф'),
    ('6', 'Хэмингуэй Э'),
    ('7', 'Голдинг У'),
    ('8', 'Дюма А'),
    ('9', 'Дойл АК')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
}
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "INSERT INTO manager VALUES
    ('400', '20', 'Макаров БК', '15000', '20000', '30', '40'),
    ('404', '20', 'Макаркин ПР', '20000', '20000', '35', '41'),
    ('401', '21', 'Макарич ВД', '20000', '23000', '32', '42'), 
    ('402', '21', 'Макареев МЛ', '15000', '23000', '33', '43'), 
    ('403', '21', 'Макарушкин ДР', '15000', '23000', '34', '44')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "INSERT INTO department VALUES 
    ('20', 'Классическая русская литература', '16+', '5000'),
    ('21', 'Классическая зарубежная литература', '16+', '1000')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "INSERT INTO seller VALUES
	('30', '21', 'Алексеев БР'), 
    ('32', '20', 'Казаков МА'), 
    ('33', '21', 'Лорина ЛС'),
    ('34', '20', 'Дмитриева ГК'), 
    ('35', '21', 'Грошева РЛ')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

$sql = "INSERT INTO cleaner VALUES
	('40', 'Леснов ПР', '08:00', 'Понедельник'), 
    ('41', 'Майский ОК', '14:00', 'Вторник'),
    ('42', 'Летний ГД', '08:00', 'Среда'),
    ('43', 'Киров ЗД', '08:00', 'Четверг'),
    ('44', 'Итров КА', '14:00', 'Пятница'),
    ('45', 'Солонин АР', '14:00', 'Суббота'),
    ('46', 'Кереев ОГ', '08:00', 'Воскресенье')";
if (mysqli_query($link, $sql)) {
    echo "Created successfully ";
} else {
    echo "Error" . mysqli_error($link);
}

mysqli_close($link);
?>