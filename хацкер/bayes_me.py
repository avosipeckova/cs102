from collections import Counter
from math import log

class NaiveBayesClassifier:

    def __init__(self, alpha=1):
        self.alpha = alpha

    def fit(self, X, y):
        """ Fit Naive Bayes classifier according to X, y. """
        labeled_words = [] #создаем список кортежей для слов и их оценочек
        for doc, label in zip(X, y):
            for word in doc.split():
                labeled_words.append((word, label))
        self.word_lab_counted = dict(Counter(labeled_words)) #словарик с подсчетом кадой пары слов в лэйбелнутых словах
        self.labels_counted = dict(Counter(y)) #сколько раз встречается данная оценка в векторе оценок

        words = [word for doc in X for word in doc.split()] #список со всеми словами
        self.words_counted = dict(Counter(words)) #сколько раз встречается каждое слово

        self.info_labels = {} #p(C) пробабилити оф класс - вероятность того, что попадется слово определенного класса

        for label in self.labels_counted:
            self.info_labels[label] = {
                'number of words': self.count_words_for_label(label),
                'apr_prob': self.labels_counted[label] / len(y)
            }
        self.info_words = {} #p(wi/C) считаем вероятность принадлежности каждого слова к оперделенному маркеру (оценке)

    def predict(self, X):
        """ Perform classification on an array of test vectors X. """
        answers = []

        for doc in X:  # строка
            words = doc.split()  # слова в строке
            predictions = []

            # ln(p(C))
            for label in self.info_labels:
                apr_prob = self.info_labels[label]['apr_prob']
                result = log(apr_prob)

                # ln(p(w_i|C))
                for word in words:
                    curr_word = self.info_words.get(word)
                    if curr_word:
                        result += log(curr_word[label])

                predictions.append((result, label))

            score, predicted = max(predictions)
            answers.append(predicted)

        return answers

    def score(self, X_test, y_test):
        """ Returns the mean accuracy on the given test data and labels. """
        total = len(y_test)
        correct = 0
        prediction = self.predict(X_test)

        for i in range(len(prediction)):
            if prediction[i] == y_test[i]:
                correct += 1

        return correct / total

    def count_words_for_label(self, label):
        c = 0
        for word, word_label in self.word_lab_counted:
            if word_label == label:
                c += self.word_lab_counted[(word, word_label)]
        return c

    def smoothing(self, word, label):

        # параметр сглаживания:
        alpha = self.alpha
        # число наблюдений слова для данного класса:
        n_ic = self.word_lab_counted.get((word, label), 0)
        # общее количество слов в классе:
        n_c = self.info_labels[label]['number_of_words']
        # размерность вектора слов:
        d = len(self.words_counted)

        return (n_ic + alpha) / (n_c + alpha * d)
